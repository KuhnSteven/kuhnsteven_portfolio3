// Steve's DnD CC&T
// Dungeons & Dragons Character Creator & Tracker
// JavaScript final project for Project Portfolio 3

// Web Design & Development
// October 1st, 2017

// This application is intended to be a web-based assistant for Dungeons &
// Dragons players. It's sole purpose is to replace the main spreadsheet so
// that a player may actively update values as gameplay continues. On the
// off-chance a DnD player forgot their dice, a die-roll mechanism is also
// intended. Beyond that, other features are loosely planned but may not see
// completion due to unfortunate circumstances in time and natural life events.

// Features:
// Character Creation
// Character Tracking
// Character Load/Save via cookies (localStorage)
// Dice System (D4, D6, D8, D10, D12, D20)
// Character Comparisons (unlikely)
// Duels (Character VS Character) (doubtful)

// VARIABLES
var _characterName = "Your Character";
var _playerName = "You";
var _charAge = 17;
var _charGender = 0;
var _charRace = "Human";
var _charAllignment = "Neutral Good";
var _charHeight = 6;
var _charWeight = 216;
var _charHairColor = "grey";

function getVariables(charName, plName, chAge, chGender, chRace, chAllign, chHeight, chWeight, chHColor)
{
	_characterName = charName;
	_playerName = plName;
	_charAge = chAge;
	_charGender = chGender;
	_charRace = chRace;
	_charAllignment = chAllign;
	_charHeight = chHeight;
	_charWeight = chWeight;
	_charHairColor = chHColor;

	saveCharacter();
}

function saveCharacter()
{
	if(typeof(Storage) !== "undefined")
		{
			prompt("saving." + _charRace);
		localStorage.setItem(key: "NAME" + _characterName, value: _characterName);
		}

	else
		{
		prompt("Unfortunately your browser does not support Web Storage.")
		}

}

function loadCharacter()
{

	if(typeof(Storage) !== "undefined")
		{

		_characterName = localStorage.getItem(key: "NAME" + _characterName);
		}

	else
		{
		prompt("Unfortunately your browser does not support Web Storage.")
		}
}